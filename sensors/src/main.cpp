// Generates sensor readings and sends them to SQS queue

#include <chrono>
#include <iostream>
#include <random>
#include <thread>

#include <aws/core/Aws.h>
#include <aws/core/utils/memory/stl/SimpleStringStream.h>
#include <aws/core/utils/json/JsonSerializer.h>

#include <aws/sqs/SQSClient.h>
#include <aws/sqs/model/GetQueueUrlRequest.h>
#include <aws/sqs/model/ListQueuesRequest.h>
#include <aws/sqs/model/SendMessageRequest.h>
#include <aws/core/utils/logging/DefaultLogSystem.h>
#include <aws/core/utils/logging/AWSLogging.h>

struct SensorReading
{
    double temperature;
    double humidity;
    double brightness;
};

Aws::String toJson(const SensorReading &reading)
{
    using namespace Aws;
    OStringStream ss;
    ss << '{'
       << "\"temperature\": " << reading.temperature << ", "
       << "\"humidity\": " << reading.humidity << ", "
       << "\"brightness\": " << reading.brightness
       << '}';
    return ss.str();
}

SensorReading generateReading()
{
    static std::random_device rd;
    static std::mt19937 engine(rd());
    static std::uniform_real_distribution<> dis(0.0, 30.0);
    return SensorReading{dis(engine), dis(engine), dis(engine)};
}

Aws::String getSensorQueueUrl(const Aws::SQS::SQSClient &sqs, const Aws::String &queueName)
{
    using namespace Aws;
    while (true)
    {
        const auto request = SQS::Model::GetQueueUrlRequest().WithQueueName(queueName);
        const auto result = sqs.GetQueueUrl(request);
        if (result.IsSuccess())
        {
            const auto queueUrl = result.GetResult().GetQueueUrl();
            std::cout << "Got Queue URL: " << queueUrl << std::endl;
            return queueUrl;
        }
        else
        {
            std::cout << "Error Getting Queue URL: " << result.GetError() << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(3000));
        }
    }
}

int main()
{
    constexpr int NUM_SENSORS = 5;
    using namespace Aws;
    std::cout << "Starting Sensors" << std::endl;
    SDKOptions options;
    options.loggingOptions.logLevel = Utils::Logging::LogLevel::Error;
    InitAPI(options);

    {
        Client::ClientConfiguration config;
        config.endpointOverride = String("localstack:4566");
        config.scheme = Http::Scheme::HTTP;
        SQS::SQSClient sqs(config);

        const String queue_url = getSensorQueueUrl(sqs, "sensor-events");

        while (true)
        {
            // generate message
            OStringStream ss;
            ss << '[';
            for (int i = 0; i < NUM_SENSORS; ++i)
            {
                auto reading{generateReading()};
                ss << toJson(reading);
                if (i < (NUM_SENSORS - 1))
                {
                    ss << ", ";
                }
            }
            ss << ']';
            String body(ss.str());

            //send message
            const auto sm_req = SQS::Model::SendMessageRequest().WithQueueUrl(queue_url).WithMessageBody(body);
            const auto sm_out = sqs.SendMessage(sm_req);

            std::this_thread::sleep_for(std::chrono::milliseconds(5000));
        }

        // Leads to fatal error with localstack - maybe because of custom endpoint?
        // TODO: fix
        // ShutdownAPI(options);
        Utils::Logging::ShutdownAWSLogging();
        return EXIT_SUCCESS;
    }
}