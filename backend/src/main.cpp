// Collects sensor readings from SQS queue

#include <chrono>
#include <iostream>
#include <thread>

#include <aws/core/Aws.h>
#include <aws/core/utils/memory/stl/SimpleStringStream.h>
#include <aws/core/utils/json/JsonSerializer.h>

#include <aws/sqs/SQSClient.h>

#include <aws/sqs/model/GetQueueUrlRequest.h>
#include <aws/sqs/model/ReceiveMessageRequest.h>
#include <aws/sqs/model/DeleteMessageRequest.h>
#include <aws/sqs/model/ListQueuesRequest.h>
#include <aws/core/utils/logging/DefaultLogSystem.h>
#include <aws/core/utils/logging/AWSLogging.h>

bool isValid(const Aws::Utils::Json::JsonView &sensorEvent)
{
    static const Aws::String keys[] = {"temperature",
                                       "humidity",
                                       "brightness"};

    if (!sensorEvent.IsObject())
        return false;

    for (const auto &k : keys)
    {
        if (!sensorEvent.KeyExists(k) || !sensorEvent.GetObject(k).IsFloatingPointType())
            return false;
    }
    return true;
}

void handleMessage(const Aws::SQS::Model::Message &message)
{
    using namespace Aws;
    constexpr char logTag[] = "Reading-Collector";
    auto *logSystem = Utils::Logging::GetLogSystem();

    const auto &body = message.GetBody();
    Utils::Json::JsonValue val(body);
    Utils::Json::JsonView view;

    if (val.WasParseSuccessful())
    {
        view = val.View();
    }
    else
    {
        logSystem->LogStream(Utils::Logging::LogLevel::Error,
                             logTag,
                             OStringStream("JSON-Parsing failed for: " + body));
        logSystem->Flush();
        return;
    }

    if (view.IsListType())
    {
        std::cout << "Got JSON list: " << std::endl;
        const auto arr = view.AsArray();
        for (size_t i = 0; i < arr.GetLength(); ++i)
        {
            if (isValid(arr[i]))
            {
                std::cout << arr[i].WriteReadable() << std::endl;
            }
            else
            {
                logSystem->LogStream(Utils::Logging::LogLevel::Error,
                                     logTag,
                                     OStringStream("Got invalid message: " + arr[i].WriteReadable()));
            }
        }
        logSystem->Flush();
    }
    else
    {
        logSystem->LogStream(Utils::Logging::LogLevel::Error,
                             logTag,
                             OStringStream("Got JSON non-list: " + view.WriteReadable()));
        logSystem->Flush();
        return;
    }
}

Aws::String getSensorQueueUrl(const Aws::SQS::SQSClient &sqs, const Aws::String &queueName)
{
    using namespace Aws;
    while (true)
    {
        const auto request = SQS::Model::GetQueueUrlRequest().WithQueueName(queueName);
        const auto result = sqs.GetQueueUrl(request);
        if (result.IsSuccess())
        {
            const auto queueUrl = result.GetResult().GetQueueUrl();
            std::cout << "Got Queue URL: " << queueUrl << std::endl;
            return queueUrl;
        }
        else
        {
            std::cout << "Error Getting Queue URL: " << result.GetError() << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(3000));
        }
    }
}

int main()
{
    using namespace Aws;
    std::cout << "Starting Reading-Collector" << std::endl;
    SDKOptions options;
    options.loggingOptions.logLevel = Utils::Logging::LogLevel::Error;
    InitAPI(options);

    {
        Client::ClientConfiguration config;
        config.endpointOverride = String("localstack:4566");
        config.scheme = Http::Scheme::HTTP;
        SQS::SQSClient sqs(config);

        const String queue_url = getSensorQueueUrl(sqs, "sensor-events");

        while (true)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(5000));

            // get message
            const auto rm_req = SQS::Model::ReceiveMessageRequest().WithQueueUrl(queue_url);
            const auto rm_out = sqs.ReceiveMessage(rm_req);
            if (!rm_out.IsSuccess())
            {
                std::cout << "Error receiving message from queue " << queue_url << ": "
                          << rm_out.GetError().GetMessage() << std::endl;
                continue;
            }
            const auto &messages = rm_out.GetResult().GetMessages();
            if (messages.size() == 0)
            {
                std::cout << "No messages received from queue " << queue_url << std::endl;
                continue;
            }
            const auto &message = messages[0];

            // process message
            handleMessage(message);

            // delete message
            const auto dm_req = SQS::Model::DeleteMessageRequest().WithQueueUrl(queue_url).WithReceiptHandle(message.GetReceiptHandle());
            const auto dm_out = sqs.DeleteMessage(dm_req);
            if (!dm_out.IsSuccess())
            {
                std::cout << "Error deleting message " << message.GetMessageId() << " from queue " << queue_url << ": " << dm_out.GetError().GetMessage() << std::endl;
            }
        }

        // Leads to fatal error with localstack - maybe because of custom endpoint?
        // TODO: fix
        // ShutdownAPI(options);
        Utils::Logging::ShutdownAWSLogging();
        return EXIT_SUCCESS;
    }
}