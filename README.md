# greenhouse
Simulates a system of generic 'greenhouse' _sensors_ that send their readings to an AWS SQS queue.
Another _collector_ service receives these messages and prints the values to console.
Sensor and collector services are implemented in C++ using the  [AWS C++ SDK](https://docs.aws.amazon.com/sdk-for-cpp/index.html) (for no good reason...).
The SQS queue is run locally with [LocalStack](https://github.com/localstack/localstack).
Requires docker and docker-compose.

__Note__: Tested on Ubuntu 20.04 with docker 20.10.6 and docker-compose 1.29.1

## Build and Run
Note: The first step includes building the AWS SDK for C++ and may take a while.
```bash
common/build.sh
docker-compose up
```
